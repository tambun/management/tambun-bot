FROM golang:1.12 AS builder

RUN apt-get update -qq && apt-get install -qq -y ca-certificates

RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR $GOPATH/src/gitlab.com/tambun/management/tambun-bot
COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure --vendor-only
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /tambun-bot .

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /tambun-bot /app/
WORKDIR /data

ENTRYPOINT ["/app/tambun-bot"]
