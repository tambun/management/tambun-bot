package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var AppConfig conf

type conf struct {
	IPProvider      string     `yaml:"ip_provider"`
	DiscordBotToken string     `yaml:"discord_bot_token"`
	ChannelID       *channelID `yaml:"channel_id"`
}

type channelID struct {
	Info    string `yaml:"info"`
	Warning string `yaml:"warning"`
	UpDown  string `yaml:"updown"`
}

func InitializeAppConfig(fileName string) (err error) {
	var yamlFile []byte
	if yamlFile, err = ioutil.ReadFile(fileName); err != nil {
		return
	}
	if err = yaml.UnmarshalStrict(yamlFile, &AppConfig); err != nil {
		return
	}

	return
}
