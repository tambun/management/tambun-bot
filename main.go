package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tambun/management/tambun-bot/config"
	"gopkg.in/robfig/cron.v3"
)

var (
	cronner   *cron.Cron
	currentIP string
)

func init() {
	var err error
	log.SetPrefix("[Tambun] ")
	if err = config.InitializeAppConfig("conf.yml"); err != nil {
		log.Fatalf("[InitializeAppConfig] failed reading conf.yml. %+v", err)
	}
	log.Println("Starting...")

	if currentIP, err = GetIP(); err != nil {
		log.Printf("[GetIP] Failed getting public IP. %+v", err)
	}
	log.Printf("[GetIP] Public IP set to %s", currentIP)

	cronner = cron.New()
}

func main() {
	var err error
	var dg *discordgo.Session
	if dg, err = discordgo.New("Bot " + config.AppConfig.DiscordBotToken); err != nil {
		log.Fatalf("[Discord] Error creating Discord session. %+v", err)
	}

	dg.AddHandler(ready)
	dg.AddHandler(messageCreate)

	if err = dg.Open(); err != nil {
		log.Fatalf("[Discord] Error opening Discord session. %+v", err)
	}

	log.Println("Tambun is now running")
	_ = SendUpDown(dg, &discordgo.MessageEmbed{
		Color:       0x2255cc,
		Timestamp:   time.Now().Format(time.RFC3339),
		Description: "Tambun Homelab running!",
		Fields: []*discordgo.MessageEmbedField{
			{"Public IP", fmt.Sprintf("`%s`", currentIP), false},
		},
	})
	cronner.Start()
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-c

	cronner.Stop()
	log.Printf("Stopping...")
	_ = SendUpDown(dg, &discordgo.MessageEmbed{
		Color:       0xdd3311,
		Timestamp:   time.Now().Format(time.RFC3339),
		Description: "Tambun Homelab stopping!",
	})
	_ = dg.Close()
}

func ready(s *discordgo.Session, event *discordgo.Ready) {
	_ = s.UpdateStatus(0, currentIP)
	InitializeCron(s)
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}
}

func InitializeCron(s *discordgo.Session) {
	_, _ = cronner.AddFunc("* * * * *", RetrieveNotifyIP(s))
}

func RetrieveNotifyIP(s *discordgo.Session) func() {
	return func() {
		newIP, err := GetIP()
		if err != nil {
			log.Printf("[GetIP] Failed getting public IP. %+v", err)
			return
		}
		if newIP != currentIP {
			log.Printf("[GetIP] Public IP changed to %s", newIP)
			_ = s.UpdateStatus(0, newIP)
			_ = SendInfo(s, &discordgo.MessageEmbed{
				Author:      &discordgo.MessageEmbedAuthor{Name: "IP Cronner"},
				Color:       0x00dd33,
				Timestamp:   time.Now().Format(time.RFC3339),
				Description: "Public IP has been updated!",
				Fields: []*discordgo.MessageEmbedField{
					{"New IP", fmt.Sprintf("`%s`", newIP), false},
				},
			})
			currentIP = newIP
		}
		_, _ = s.ChannelEditComplex(config.AppConfig.ChannelID.UpDown, &discordgo.ChannelEdit{
			Topic: fmt.Sprintf("Last IP check: %s", time.Now().Format(time.RFC822)),
		})
	}
}

func SendInfo(s *discordgo.Session, msg *discordgo.MessageEmbed) (err error) {
	_, err = s.ChannelMessageSendEmbed(config.AppConfig.ChannelID.Info, msg)
	return
}

func SendWarning(s *discordgo.Session, msg *discordgo.MessageEmbed) (err error) {
	_, err = s.ChannelMessageSendEmbed(config.AppConfig.ChannelID.Warning, msg)
	return
}

func SendUpDown(s *discordgo.Session, msg *discordgo.MessageEmbed) (err error) {
	_, err = s.ChannelMessageSendEmbed(config.AppConfig.ChannelID.UpDown, msg)
	return
}

func GetIP() (ip string, err error) {
	var resp *http.Response
	if resp, err = http.Get(config.AppConfig.IPProvider); err != nil {
		return
	}
	defer resp.Body.Close()
	var body []byte
	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	ip = strings.TrimSpace(string(body))

	return
}
